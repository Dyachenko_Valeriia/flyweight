import java.util.HashMap;
import java.util.Map;

public class WineFactory {
    static Map<String, WineType> wineTypes = new HashMap<>();

    public static WineType getWineType(String name, Color color, int year, int strength){
        WineType result = wineTypes.get(name);
        if(result == null){
            result = new WineType(name, color, year, strength);
            wineTypes.put(name, result);
        }
        return result;
    }
}
