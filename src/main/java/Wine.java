public class Wine {
    private int number; //номер ячейки на складе
    private WineType wineType;

    public Wine(int number, WineType wineType) {
        this.number = number;
        this.wineType = wineType;
    }
}
