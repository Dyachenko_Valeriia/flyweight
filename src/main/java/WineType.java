import java.util.Objects;

public class WineType {
    private String name;
    private Color color;
    private int year;
    private int strength;

    public WineType(String name, Color color, int year, int strength) {
        this.name = name;
        this.color = color;
        this.year = year;
        this.strength = strength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WineType wineType = (WineType) o;
        return year == wineType.year &&
                strength == wineType.strength &&
                Objects.equals(name, wineType.name) &&
                color == wineType.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, color, year, strength);
    }
}
