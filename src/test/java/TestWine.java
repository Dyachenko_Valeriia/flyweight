import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestWine {
    @Test
    public void testWine(){
        WineType wineType = new WineType("Каберне Совиньон", Color.RED, 1986, 12);
        assertEquals(wineType, WineFactory.getWineType("Каберне Совиньон", Color.RED, 1986, 12));
    }
}
